# frozen_string_literal: true

require_relative 'bootstrap'

delivery_service = DeliveryService.new(
  [
    Bike.new,
    Bike.new,
    Bike.new,
    Car.new('AE4556'),
    Car.new('AE2885')
  ]
)

delivery_weight = 10
delivery_distance = 40

available_transport = delivery_service.find_transport(delivery_weight,
                                                      delivery_distance)

if available_transport
  puts "The available transport has found: #{available_transport}"

  if available_transport.respond_to? :reg_number
    puts "Transport registration number: #{available_transport.reg_number}"
  end

  puts 'Estimated delivery time: '\
    "#{available_transport.delivery_time(delivery_distance)} hour(s)"

  available_transport.unavailable!
  delivery_service.delivery_confirmation
  available_transport.available!
end
