# frozen_string_literal: true

# Defines class Car inherits from Transport
#
# Accepts `reg_number` as string
#
# Readable attributes: :max_weight, :speed and :reg_number
# Accessible attributes: :available
#
# Default values of :max_weight is 100, :speed is 50
class Car < Transport
  attr_reader :reg_number

  def initialize(reg_number)
    @max_weight = 100
    @speed = 50
    @available = true
    @reg_number = reg_number
  end
end
