# frozen_string_literal: true

# Defines abstract class Transport
#
# Readable attributes: :max_weight, :speed
# Accessible attributes: :available
class Transport
  include Comparable

  attr_accessor :available
  attr_reader :max_weight, :speed

  def initialize
    raise NotImplementedError
  end

  def <=>(other)
    speed <=> other.speed
  end

  def find_available(weight, _distance)
    self if available && max_weight >= weight
  end

  def available!
    self.available = true
  end

  def unavailable!
    self.available = false
  end

  def delivery_time(distance)
    distance / speed.to_f
  end
end
