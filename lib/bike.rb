# frozen_string_literal: true

# Defines class Bike inherits from Transport
#
# Readable attributes: :max_weight, :speed and :max_distance
# Accessible attributes: :available
#
# Default values of :max_weight is 10, :speed is 10 and :max_distance is 30
class Bike < Transport
  attr_reader :max_distance

  def initialize
    @max_weight = 10
    @speed = 10
    @available = true
    @max_distance = 30
  end

  def find_available(weight, distance)
    self if super(weight, distance) && max_distance >= distance
  end
end
