# frozen_string_literal: true

# Defines class DeliveryService
#
# Accepts `pool` as array
class DeliveryService
  def initialize(pool)
    @pool = pool
  end

  def find_transport(weight, distance)
    @pool.find do |transport|
      transport.find_available(weight, distance)
    end
  end

  def delivery_confirmation
    puts 'Package has delivered!'
  end
end
