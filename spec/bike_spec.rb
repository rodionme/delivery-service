# frozen_string_literal: true

describe Bike do
  let(:weight) { subject.max_weight }
  let(:distance_appropriate) { subject.max_distance }
  let(:distance_too_long) { subject.max_distance + 1 }

  it 'inherits class Transport' do
    expect(subject).to be_a Transport
  end

  it 'should move with a maximum speed of 10 kph' do
    expect(subject.speed).to equal 10
  end

  it 'should carry a maximum weight of 10 kg' do
    expect(subject.max_weight).to equal 10
  end

  it 'should have a maximum distance of 30 km' do
    expect(subject.max_distance).to equal 30
  end

  describe '#find_available' do
    it 'returns Bike which meet the criteria' do
      expect(subject.find_available(weight, distance_appropriate)).to be_a Bike
      expect(subject.find_available(weight, distance_too_long)).to be_nil
    end
  end
end
