# frozen_string_literal: true

describe Car do
  subject { described_class.new('reg_number') }

  it 'inherits class Transport' do
    expect(subject).to be_a Transport
  end

  it 'should move with a maximum speed of 50 kph' do
    expect(subject.speed).to equal 50
  end

  it 'should carry a maximum weight of 100 kg' do
    expect(subject.max_weight).to equal 100
  end

  it { is_expected.to respond_to :reg_number }
end
