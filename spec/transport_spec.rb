# frozen_string_literal: true

describe Transport do
  it 'should not be initialized' do
    expect { subject }.to raise_error(NotImplementedError)
  end
end

class DummyTransport < Transport
  attr_reader :available, :max_weight

  def initialize(speed, max_weight = 100)
    @speed = speed
    @max_weight = max_weight
    @available = true
  end
end

describe DummyTransport do
  let(:transport1) { DummyTransport.new(10) }
  let(:transport2) { DummyTransport.new(11) }
  let(:transport3) { DummyTransport.new(10) }

  let(:weight_appropriate) { subject.max_weight }
  let(:weight_too_heavy) { subject.max_weight + 1 }
  let(:distance) { 10 }

  subject { described_class.new(10) }

  describe '.<=>' do
    it 'should compare transports between each other' do
      expect(transport1 == transport3).to be_truthy
      expect(transport1 == transport2).to be_falsey
      expect(transport1 < transport2).to be_truthy
    end
  end

  describe '#find_available' do
    it 'returns Transport which meet the criteria' do
      expect(subject.find_available(weight_appropriate, distance)).to be_a Transport
      expect(subject.find_available(weight_too_heavy, distance)).to be_nil
    end
  end

  describe '#available!' do
    before do
      subject.available = false
    end

    it 'should make Transport available' do
      expect { subject.available! }.to change { subject.available }.from(false).to(true)
    end
  end

  describe '#unavailable!' do
    before do
      subject.available = true
    end

    it 'should make Transport unavailable' do
      expect { subject.unavailable! }.to change { subject.available }.from(true).to(false)
    end
  end

  describe '#delivery_time' do
    let(:distance) { 20 }

    it 'should calculate delivery time correctly' do
      expect(subject.delivery_time(distance)).to equal 2.0
    end
  end
end
