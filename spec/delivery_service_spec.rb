# frozen_string_literal: true

describe DeliveryService do
  let(:available_bike) do
    @bike = Bike.new
    @bike.available!
    @bike
  end

  let(:unavailable_bike) do
    @bike = Bike.new
    @bike.unavailable!
    @bike
  end

  let(:available_car) do
    @car = Car.new('AE4556')
    @car.available!
    @car
  end

  let(:unavailable_car) do
    @car = Car.new('AE2885')
    @car.unavailable!
    @car
  end

  let(:pool) do
    [
      unavailable_bike,
      available_bike,
      unavailable_car,
      available_car
    ]
  end

  let(:weight_for_all_kind) { 10 }
  let(:weight_for_cars_only) { 11 }
  let(:weight_too_heavy) { 101 }

  let(:distance_for_all_kind) { 30 }
  let(:distance_for_cars_only) { 31 }

  subject do
    described_class.new(pool)
  end

  it { is_expected.to respond_to :delivery_confirmation }

  describe '#find_transport' do
    it 'should take 2 arguments' do
      expect(subject.method(:find_transport).arity).to equal 2
    end

    context 'with a weight and delivery distance suitable for any type of transport' do
      it 'returns the first available Transport' do
        expect(subject.find_transport(weight_for_all_kind, distance_for_all_kind)).to equal(available_bike).or equal(available_car)
      end
    end

    context 'with weight and delivery distance suitable only for a car' do
      it 'returns the first available Car' do
        expect(subject.find_transport(weight_for_cars_only, distance_for_cars_only)).to equal available_car
      end
    end

    context 'with too heavy weight' do
      it 'returns nothing' do
        expect(subject.find_transport(weight_too_heavy, distance_for_all_kind)).to be_nil
      end
    end

    context 'with too long delivery distance for Bike' do
      it 'returns the first available Car' do
        expect(subject.find_transport(weight_for_all_kind, distance_for_cars_only)).to equal available_car
      end
    end
  end

  context '#delivery_confirmation' do
    it 'prints message of successful delivery' do
      expect { subject.delivery_confirmation }.to output("Package has delivered!\n").to_stdout
    end
  end
end
